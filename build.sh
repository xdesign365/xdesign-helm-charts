#!/bin/bash

BUILD_DIR="$PWD/build"
CHART_BUCKET="xdesign-helm-charts"

if [ ! -d $BUILD_DIR ]; then
    mkdir $BUILD_DIR
fi

cd $BUILD_DIR

for CHART in ../charts/*; do
    NAME=$(basename $CHART)

    CHART_PATH=$(helm package $CHART | sed 's|Successfully packaged chart and saved it to: ||g')
    CHART_NAME=$(basename $CHART_PATH)

    echo "Checking: /charts/$CHART_NAME"

    OBJECT_VERSION=$(aws s3api head-object --bucket $CHART_BUCKET --key "charts/$CHART_NAME" 2>/dev/null)

    if [[ -z $OBJECT_VERSION ]]; then
        echo "$CHART_NAME not found on remote; pushing"
        aws s3 cp "$CHART_NAME" "s3://xdesign-helm-charts/charts/$CHART_NAME"
    fi
done

INDEX_REMOTE="s3://xdesign-helm-charts/charts/index.yaml"

aws s3 cp $INDEX_REMOTE old.yaml

helm repo index . --merge old.yaml

aws s3 cp index.yaml $INDEX_REMOTE

helm repo update

helm search repo xdesign